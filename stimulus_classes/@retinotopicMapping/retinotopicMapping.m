classdef retinotopicMapping < handle
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        sphericalCorrection
        trialsPerDirection
        sweeps
        period
        stimWidth
        stimTextures
        displayInfo
        outputLine
        trialOrder
        currentRound
        currentImageID
        currentTrial
        flipTime
        FPS
        ifi
        squareSize
        squareFlipT
        framesPerSweep
    end
    
    methods
        function obj = retinotopicMapping(presentationData, outputLine, flipT)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            obj.sphericalCorrection = presentationData.sphericalCorrection;
            obj.trialsPerDirection = presentationData.trialsPerDirection;
            obj.sweeps = presentationData.sweeps;
            obj.period = presentationData.period;
            obj.stimWidth = presentationData.stimWidth;
            obj.trialOrder = nan(presentationData.trialsPerDirection*4,1);
            obj.displayInfo = presentationData.displayInfo;
            obj.outputLine = outputLine;
            obj.flipTime = flipT;
            
            obj.FPS = presentationData.FPS;
            obj.ifi = 1/obj.FPS;
            obj.squareSize = presentationData.squareSize;
            obj.squareFlipT = presentationData.squareFlipT; % ms
            
            
            obj.prepareStimuli;
            
            obj.currentTrial = 0;
            obj.prepareStimOrder;
            %             obj.prepareNextStimulus;
        end
        
        function prepareStimuli(obj)
            
            if obj.sphericalCorrection
                
                % Initial settings
                monW = obj.displayInfo.screenSettings.monitorWidth;
                widthPixels = obj.displayInfo.screenSettings.disp(1);
                monH = obj.displayInfo.screenSettings.monitorHeight;
                heightPixels = obj.displayInfo.screenSettings.disp(2);
                faceDist = obj.displayInfo.screenSettings.monitorDistance;
                
                monitorWidth = linspace(-monW/2, monW/2, widthPixels); % Where monW is the width of the monitor in cm
                monitorHeight = linspace(-monH/2, monH/2, heightPixels);
                
                x = faceDist;
                y = monitorWidth;
                z = monitorHeight;
                %                 barWidth = obj.stimWidth * obj.displayInfo.screenSettings.pixPerDeg; % Degrees
                
                %                 driftSpeedHoriz = widthPixels + barWidth./obj.period; % Pixels per second
                %                 driftSpeedVert = screenHeight + barWidth./obj.period;
                
                %                 driftPerFrame = driftSpeed * obj.ifi;
                
                % Testing just vertical now
                % - get degrees background
                
                S = nan(length(z), length(y),2);
                
                for theta = 1:length(y)
                    for phi = 1:length(z)
                        
                        S(phi, theta, :) = [atan(-y(theta)/x), (pi/2) - acos(z(phi)/sqrt(x^2 + y(theta)^2 + z(phi)^2))];
                    end
                end
                
                vertAngles = rad2deg(S(:,:,2));
                horzAngles = rad2deg(S(:,:,1));
                vertChecks = abs(mod(floor(vertAngles / obj.squareSize),2));
                horzChecks = abs(mod(floor(horzAngles / obj.squareSize),2));
                comboChecks = mod(vertChecks + horzChecks,2);
                comboChecks(:,:,2) = abs(comboChecks-1);
                
                comboChecks(comboChecks == 0) = obj.displayInfo.black;
                comboChecks(comboChecks == 1) = obj.displayInfo.white;
                
                obj.framesPerSweep = ceil(obj.period * obj.FPS);
                obj.stimTextures = cell(obj.framesPerSweep, 2);%, 2);
                
                %% Make vertical stimuli first
                distanceToCover = max(vertAngles, [], 'all') - min(vertAngles,[], 'all');
                
                driftPerFrame = (distanceToCover + obj.stimWidth) / obj.framesPerSweep;
                startAngle = min(vertAngles,[], 'all');
                
                % Gotta add the code to make it a psychtoolbox texture -
                % not sure if extra arguments are needed
                chosenChecks = 1;
                squareTime = 0;

                for ii = 1:obj.framesPerSweep
                    displaySection = vertAngles <= startAngle & vertAngles > startAngle-obj.stimWidth;
                    
                    if squareTime >= obj.squareFlipT
                        chosenChecks = abs(chosenChecks - 3);
                        squareTime = 0;
                    end
                    squareTime = squareTime + obj.ifi;
                    
                    firstCheck = comboChecks(:,:,chosenChecks);
                    
                    firstCheck(~displaySection) = obj.displayInfo.grey;

                    obj.stimTextures{ii,1} = Screen('MakeTexture', obj.displayInfo.winPrimary, firstCheck);

                    startAngle = startAngle + driftPerFrame;
                end
                
                % Make horizontal
                
                distanceToCover = max(horzAngles, [], 'all') - min(horzAngles,[], 'all');
                
                driftPerFrame = (distanceToCover + obj.stimWidth) / obj.framesPerSweep;
                startAngle = min(horzAngles,[], 'all');
                
                chosenChecks = 1;
                squareTime = 0;
                
                for ii = 1:obj.framesPerSweep
                    displaySection = horzAngles <= startAngle & horzAngles > startAngle-20;
                    
                    if squareTime >= obj.squareFlipT
                        chosenChecks = abs(chosenChecks - 3);
                        squareTime = 0;
                    end
                    squareTime = squareTime + obj.ifi;
                    
                    firstCheck = comboChecks(:,:,chosenChecks);
          
                    firstCheck(~displaySection) = obj.displayInfo.grey;

                    obj.stimTextures{ii,2} = Screen('MakeTexture', obj.displayInfo.winPrimary, firstCheck);
                    startAngle = startAngle + driftPerFrame;
                end
                
            else
                
                
            end
        end
        
        function prepareStimOrder(obj)
            allTrials = repmat([1;-1;2;-2], [obj.trialsPerDirection, 1]);
            obj.trialOrder = allTrials(randperm(length(allTrials)));
        end
        
        function presentNextStimulus(obj)
            obj.currentTrial = obj.currentTrial + 1;
            
            
            currentStimulus = obj.stimTextures(:,abs(obj.trialOrder(obj.currentTrial)));
            if obj.trialOrder(obj.currentTrial) < 0
                currentStimulus = flip(currentStimulus);
            end
            
            for ii = 1:obj.sweeps
                for ss = 1:obj.framesPerSweep
                    
                    
                    % Draw the image to the screen, unless otherwise specified PTB will draw
                    % the texture full size in the center of the screen. We first draw the
                    % image in its correct orientation.
                    Screen('DrawTexture', obj.displayInfo.winPrimary, currentStimulus{ss});
                    
                    %             % Draw photodiode square
                    %             if obj.interTrialTime ~= 0 % If there is an ITI
                    %                 photoDiSel = (photoDiSel-3)*-1;
                    if ss < 10
                        Screen('FillRect', obj.displayInfo.winPrimary, obj.displayInfo.black, obj.displayInfo.photodiodeSquare)
                    else
                        Screen('FillRect', obj.displayInfo.winPrimary, obj.displayInfo.white, obj.displayInfo.photodiodeSquare)
                    end
                    %             else %If there is an ITI
                    %                 Screen('FillRect', cfg.displayInfo.winPrimary, cfg.displayInfo.white, photodiodeSquare)
                    %             end
                    
                    % Flip to the screen
                    obj.flipTime(end+1,1) = Screen('Flip', obj.displayInfo.winPrimary, obj.flipTime(end)+obj.ifi);
                end
                %             obj.outputLine.outputSingleScan(1);
                
            end
        end
        
    end
end
