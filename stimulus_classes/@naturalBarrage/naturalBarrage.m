classdef naturalBarrage < handle
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        imagesLocation
        imagePaths
        imageIDs
        loadedImages
        imageTextures
        destRects
        numOfImages
        numOfRounds
        stimDispTime
        interTrialTime
        interRoundTime
        imageHeight
        Xoffset
        Yoffset
        displayInfo
        outputLine
        roundDisplayOrder
        currentRound
        currentImageID
        currentTrial
        tbtRound
        tbtImageID
        tbtTrialNum
        flipTime
    end
    
    methods
        function obj = naturalBarrage(presentationData, outputLine, flipT)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            obj.imagesLocation = presentationData.imageLocation;
            obj.stimDispTime = presentationData.stimDispT;
            obj.interTrialTime = presentationData.interTrialInterval;
            obj.interRoundTime = presentationData.interRoundInterval;
            obj.numOfRounds = presentationData.numOfRounds;
            obj.roundDisplayOrder = cell(obj.numOfRounds,1);
            obj.imageHeight = presentationData.imageHeight;
            obj.Xoffset = presentationData.Xoffset;
            obj.Yoffset = presentationData.Yoffset;
            obj.displayInfo = presentationData.displayInfo;
            obj.outputLine = outputLine;
            obj.flipTime = flipT;
            obj.prepareImages;
            
            
            obj.currentRound = 1;
            obj.currentTrial = 0;
            obj.currentImageID = nan;
            obj.prepareRound;
            obj.prepareNextStimulus;
        end
        
        function prepareImages(obj)
            %Make the list of images:
            imageDir = dir(obj.imagesLocation);
            dirIndex = find(~[imageDir.isdir])';
            
            %Sort the images so they are ordered in number order
            imageList = arrayfun(@(x) fullfile(imageDir(x).folder, imageDir(x).name), dirIndex, 'uni', 0);
            imageName = arrayfun(@(x) regexp(imageDir(x).name, ['\.'], 'split'), dirIndex, 'uni', 0);
            
            for ii = 1:length(imageName)
                imageList{ii,2} = str2double(imageName{ii,1}{1,1});
            end
            imageList = sortrows(imageList,2);
            
            obj.imagePaths = imageList(:,1);
            obj.imageIDs = imageList(:,2);
            obj.numOfImages = length(obj.imageIDs);
            
            %Calculate whether a shift of the stimuli is needed
            if abs(obj.Xoffset) <=1
                xShift = obj.Xoffset*obj.displayInfo.screenSettings.centre(1);
            else
                xShift = obj.Xoffset;
            end
            if abs(obj.Yoffset) <=1
                yShift = obj.Yoffset*obj.displayInfo.screenSettings.centre(2);
            else
                yShift = obj.Yoffset;
            end
            
            
            % Load all the images
            for jj = 1:length(obj.imagePaths)
                obj.loadedImages{jj,1} = imread(obj.imagePaths{jj});
                
                % Make textures for all the images
                % Get the size of the image
                [s1, s2, s3] = size(obj.loadedImages{jj,1});
                aspectRatio = s2 / s1;
                
                imageWidth = round(obj.imageHeight*aspectRatio);
                
                %Rescaling the image
                theRect = [0 0 imageWidth, obj.imageHeight];
                obj.destRects{jj} = CenterRectOnPointd(theRect, obj.displayInfo.screenSettings.centre(1)+xShift, obj.displayInfo.screenSettings.centre(2)+yShift);
                
                % Make the image into a texture
                obj.imageTextures{jj,1} = Screen('MakeTexture', obj.displayInfo.winPrimary, obj.loadedImages{jj,1});
            end
        end
        
        function prepareRound(obj)
            obj.roundDisplayOrder{obj.currentRound,1} = randperm(obj.numOfImages)';
        end
        
        function presentNextStimulus(obj)
            
            % Draw the image to the screen, unless otherwise specified PTB will draw
            % the texture full size in the center of the screen. We first draw the
            % image in its correct orientation.
            Screen('DrawTexture', obj.displayInfo.winPrimary, obj.imageTextures{obj.currentImageID}, [], obj.destRects{obj.currentImageID}, 0);
            
            %             % Draw photodiode square
            %             if obj.interTrialTime ~= 0 % If there is an ITI
            %                 photoDiSel = (photoDiSel-3)*-1;
            Screen('FillRect', obj.displayInfo.winPrimary, obj.displayInfo.white, obj.displayInfo.photodiodeSquare)
            %             else %If there is an ITI
            %                 Screen('FillRect', cfg.displayInfo.winPrimary, cfg.displayInfo.white, photodiodeSquare)
            %             end
            
            % Flip to the screen
            if obj.currentTrial == 1 && obj.currentRound ~= 1
                obj.flipTime(end+1,1) = Screen('Flip', obj.displayInfo.winPrimary, obj.flipTime(end)+obj.interRoundTime);
            else
                obj.flipTime(end+1,1) = Screen('Flip', obj.displayInfo.winPrimary, obj.flipTime(end)+obj.interTrialTime);
            end
            obj.outputLine.outputSingleScan(1);
            
            %Inter stimulus interval
            if obj.interTrialTime > 0
                %                  photoDiSel = (photoDiSel-3)*-1;
                Screen('FillRect', obj.displayInfo.winPrimary, obj.displayInfo.black, obj.displayInfo.photodiodeSquare)
                obj.flipTime(end+1,1) = Screen('Flip', obj.displayInfo.winPrimary, obj.flipTime(end)+obj.stimDispTime);
                obj.outputLine.outputSingleScan(0);
                %cfg.flipT(end+1) = flipT;
            end
            
            obj.prepareNextStimulus;
        end
        
        function prepareNextStimulus(obj)
            if obj.currentTrial == obj.numOfImages
                obj.currentTrial = 1;
                
                obj.currentRound = obj.currentRound+1;
                obj.prepareRound;
            else
                obj.currentTrial = obj.currentTrial+1;
            end
            
            % Setup the next stimulus
            obj.currentImageID = obj.roundDisplayOrder{obj.currentRound,1}(obj.currentTrial);
            
            obj.tbtRound(end+1,1) = obj.currentRound;
            obj.tbtImageID(end+1,1) = obj.currentImageID;
            obj.tbtTrialNum(end+1,1) = obj.currentTrial;
        end
        
        function delayBetweenRounds(obj)
            Screen('FillRect', obj.displayInfo.winPrimary, obj.displayInfo.black, obj.displayInfo.photodiodeSquare)
            obj.flipTime(end+1,1) = Screen('Flip', obj.displayInfo.winPrimary, obj.flipTime(end)+obj.interTrialTime);
        end
    end
end

