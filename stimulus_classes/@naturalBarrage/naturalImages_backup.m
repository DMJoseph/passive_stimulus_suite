app.stopPresentation = 0;

% Used to locate the natural images
currentFilePath = mfilename('fullpath');
splitFile = regexp(currentFilePath, 'passiveStimSuiteApp', 'split');
naturalImageLocation = fullfile(splitFile{1}, 'cropped_images');

app.presentationData.imageLocation = naturalImageLocation;
app.presentationData.numOfRounds = app.NoofroundsEditField.Value;
app.presentationData.stimDispT = app.StimdisptimeEditField.Value;
app.presentationData.interTrialInterval = app.InterstimtimeEditField.Value;
app.presentationData.imageHeight = app.ImagediameterEditField.Value;
app.presentationData.interRoundInterval = app.InterroundtimeEditField.Value;
app.presentationData.displayInfo = app.displayInfo;

cfg = app.presentationData;
% Saved things
app.presentationData.trialNumber = [];
app.presentationData.imageID = [];
app.presentationData.roundNum = [];
app.presentationData.interRound = [];
app.presentationData.flipT = [];

% % %Make the list of images:
% % imageDir = dir(cfg.imageLocation);
% % dirIndex = find(~[imageDir.isdir])';
% % 
% % %Sort the images so they are ordered in number order
% % imageList = arrayfun(@(x) fullfile(imageDir(x).folder, imageDir(x).name), dirIndex, 'uni', 0);
% % imageName = arrayfun(@(x) regexp(imageDir(x).name, ['\.'], 'split'), dirIndex, 'uni', 0);
% % 
% % for ii = 1:length(imageName)
% %     imageList{ii,2} = str2num(imageName{ii,1}{1,1});
% % end
% % imageList = sortrows(imageList,2);
% % imageList(:,2) = [];
% % 
% % %Save image list
% % cfg.imageList = imageList;

trialNum = 0;


% Enable alpha blending for anti-aliasing
Screen('BlendFunction', cfg.displayInfo.winLeft, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% % photodiodeSquare = [0 0 120 120];
% % photoDiOpt = [cfg.displayInfo.white, cfg.displayInfo.black];
% % photoDiSel = 2;

% Sync us and get a time stamp
Screen('FillRect', cfg.displayInfo.winLeft, photoDiOpt(2), photodiodeSquare)
flipT = Screen('Flip', cfg.displayInfo.winLeft);

% Maximum priority level
topPriorityLevel = MaxPriority(cfg.displayInfo.winLeft);
Priority(topPriorityLevel);


%% Image presentation time!

% Flipping the screen to get a fresh vbl
Screen('FillRect', cfg.displayInfo.winLeft, photoDiOpt(2), photodiodeSquare)
flipT = Screen('Flip', cfg.displayInfo.winLeft);

for roundNum = 1:cfg.numOfRounds
% %     numOfImages = length(imageList);
% %     stimOrder = randperm(numOfImages)';
    
    for stimNum = 1:numOfImages
        
        app.CurrentroundEditField.Value = roundNum;
        app.CurrenttrialEditField.Value = stimNum;
        app.CurrentimageEditField.Value = stimOrder(stimNum);
        drawnow()
        
% %         currentImage = imread(imageList{stimOrder(stimNum)});
% %         
% %         % Get the size of the image
% %         [s1, s2, s3] = size(currentImage);
% %         aspectRatio = s2 / s1;
% %         
% %         imageWidth = round(cfg.imageHeight*aspectRatio);
% %         
% %         %Rescaling the image
% %         theRect = [0 0 imageWidth, cfg.imageHeight];
% %         destRect = CenterRectOnPointd(theRect, cfg.displayInfo.screenSettings.centre(1), cfg.displayInfo.screenSettings.centre(2));
% %         
% %         % Make the image into a texture
% %         imageTexture = Screen('MakeTexture', cfg.displayInfo.winLeft, currentImage);
        
% %         % Draw the image to the screen, unless otherwise specified PTB will draw
% %         % the texture full size in the center of the screen. We first draw the
% %         % image in its correct orientation.
% %         Screen('DrawTexture', cfg.displayInfo.winLeft, imageTexture, [], destRect, 0);
% %         
% %         % Draw photodiode square
% %         if cfg.interTrialInterval == 0 % If there isn't an ITI
% %             photoDiSel = (photoDiSel-3)*-1;
% %             Screen('FillRect', cfg.displayInfo.winLeft, photoDiOpt(photoDiSel), photodiodeSquare)
% %         else %If there is an ITI
% %             Screen('FillRect', cfg.displayInfo.winLeft, cfg.displayInfo.white, photodiodeSquare)
% %         end
% %         
% %         % Flip to the screen
% %         %app.outputLine.outputSingleScan(1);
% %         flipT = Screen('Flip', cfg.displayInfo.winLeft, flipT+cfg.stimDispT);
% %         
        % Log trial data
        trialNum = trialNum +1;
        app.presentationData.trialNumber(trialNum) = trialNum;
        app.presentationData.imageID(trialNum) = stimOrder(stimNum);
        app.presentationData.roundNum(trialNum) = roundNum;
        app.presentationData.interRound(trialNum) = 0;
        app.presentationData.flipT(end+1) = flipT;
% %         
% %         
% %         %Inter stimulus interval
% %         if cfg.interTrialInterval > 0
% %             photoDiSel = (photoDiSel-3)*-1;
% %             Screen('FillRect', cfg.displayInfo.winLeft, photoDiOpt(photoDiSel), photodiodeSquare)
% %             %app.outputLine.outputSingleScan(0);
% %             flipT = Screen('Flip', cfg.displayInfo.winLeft, flipT+cfg.interTrialInterval);
% %             %cfg.flipT(end+1) = flipT;
% %         end
        
        
        % Check to see if quitting
        if app.stopPresentation ~= 0
            fprintf('Quitting stimulus presentation \n')
            fprintf('Saving data... \n')
            saveData = app.presentationData;
            save(savePath, 'saveData', '-mat')
            fprintf('Data saved. \n')
            return
        end
        
        
    end % Stimulus loop
    
% %     % Inter round grey screen
% %     if cfg.interRoundInterval ~= 0
% %         %Inter round grey
% %         photoDiSel = (photoDiSel-3)*-1;
% %         Screen('FillRect', cfg.displayInfo.winLeft, photoDiOpt(photoDiSel), photodiodeSquare)
% %         flipT = Screen('Flip', cfg.displayInfo.winLeft, flipT+cfg.interRoundInterval);
% %         %                     cfg.flipT(end+1) = flipT;
% %         
% %         trialNum = trialNum +1;
% %         cfg.trialNumber(trialNum) = trialNum;
% %         cfg.imageID(trialNum) = nan;
% %         cfg.roundNum(trialNum) = roundNum;
% %         cfg.interRound(trialNum) = 1;
% %         %                     cfg.flipT(end+1) = flipT;
% %         
% %     end
    
    %Save data
    saveData = app.presentationData;
    save(savePath, 'saveData', '-mat')
    
end % Round loop

%Save data
fprintf('Saving data... \n')
saveData = app.presentationData;
save(savePath, 'saveData', '-mat')
fprintf('Data saved, session complete. \n')