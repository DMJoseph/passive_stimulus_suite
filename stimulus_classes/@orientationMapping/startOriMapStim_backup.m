function startOriMapStim(app, savePath)
% Collect stimlus details
config.spatialFreq = app.SpatialfreqEditField.Value;
config.temporalFreq = app.TemporalfreqEditField.Value;
config.contrast = app.ContrastEditField.Value;
config.stimOffsetX = app.XcentreEditField.Value;
config.stimOffsetY = app.YcentreEditField.Value;

stimTime = app.StimdurationEditField.Value;
stimMeanAdded = app.StimmeanaddEditField.Value;
interTrialTime = app.InterstimintervalEditField.Value;
interTrialAdded = app.IntervalmeanaddEditField.Value;

selectedOriMode = app.OrientationselectionButtonGroup.SelectedObject;
if strcmp(selectedOriMode.Text, 'Automatic')
    anglePallete = 0:app.OrientationdifferenceEditField.Value:360-app.OrientationdifferenceEditField.Value;
    if length(anglePallete) ~= app.NoofstimuliSpinner.Value
        warning('Mismatch in number of orientations.')
    end
else
    % Come check this works - DMJ
    oriList = app.OrientationlistEditField.Value;
    oriList = regexp(oriList, ' ', 'split');
    anglePallete = cellfun(@str2double, oriList);
end

if strcmpi(app.BlanktrialsSwitch.Value, 'On')
    anglePallete(end+1) = nan;
end

sizeList = app.StimsizeEditField.Value;
if contains(sizeList, ':')
    sizeList = num2str(eval(sizeList));
end

sizeList = regexp(sizeList, ' ', 'split');
sizePallete = cellfun(@str2double, sizeList);
sizePallete = sizePallete(~isnan(sizePallete));

app.stopPresentation = 0;

app.presentationData.spatialFreq = config.spatialFreq;
app.presentationData.temporalFreq = config.temporalFreq;
app.presentationData.contrast = config.contrast;
app.presentationData.stimOffsetX = config.stimOffsetX;
app.presentationData.stimOffsetY = config.stimOffsetY;
app.presentationData.numOfMonitors = isfield(app.displayInfo, 'winRight')+1;
app.presentationData.minStimTime = stimTime;
app.presentationData.meanStimTimeAdded = stimMeanAdded;
app.presentationData.minInterTrialTime = interTrialTime;
app.presentationData.interTrialTimeAdded = interTrialAdded;
app.presentationData.anglePallete = anglePallete;
app.presentationData.sizePallete = sizePallete;
app.presentationData.displayInfo = app.displayInfo;


% Initialise the starting parameters from the gui fields
autoStop = strcmpi(app.AutostopSwitch.Value, 'On');
trialLimit = app.TriallimitEditField.Value;

% Set up the trialwise variable trackers
trialNum = 0;

% Run the trial loop
while app.stopPresentation == 0
    trialNum = trialNum + 1;
    
    
    % Set up the stimulus presentation parameters.
    config.stimTime = stimTime + exprnd(stimMeanAdded);
    config.orientation = anglePallete(randperm(length(anglePallete),1));
    config.stimSize = sizePallete(randperm(length(sizePallete),1));
    
    % Update current trial data
    app.CurrenttrialnumEditField.Value = num2str(trialNum);
    app.CurrentorientationEditField.Value = num2str(config.orientation);
    app.CurrentsizeEditField.Value = num2str(config.stimSize);
    drawnow()
    
    app.presentationData.stimTime(trialNum,1) = config.stimTime;
    app.presentationData.orientation(trialNum,1) = config.orientation;
    app.presentationData.stimSize(trialNum,1) = config.stimSize;
    
    % Present the stimulus - one trial
    stim.singleOrimapStimPresentation(app.displayInfo, config, app.outputLine);
    
    % Pause for the inter-trial interval
    interTrialInterval = interTrialTime + exprnd(interTrialAdded);
    app.presentationData.interTrialInterval(trialNum,1) = interTrialInterval;
    pause(interTrialInterval)
    
    % Increment the trial, check whether the session should stop, and save.
    saveData = app.presentationData;
    % Save params need fixing - DMJ
    save(savePath, 'saveData', '-mat')
    if autoStop && (trialNum>=trialLimit)
        app.stopPresentation = 1;
        fprintf('Trial limit reached. Stopping stimulus presentationa and saving. \n')
    end
    
    
end

fprintf('Orientation mapping ended. Saving session. \n')
save(savePath, 'saveData', '-mat')

fprintf('Orientation mapping stopped. Session data saved. \n')
app.StartButton.Enable = 1;
app.StopButton.Enable = 0;
end