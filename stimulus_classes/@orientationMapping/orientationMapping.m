classdef orientationMapping < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        spatialFreq
        temporalFreq
        circular
        solidCircle
        contrast
        stimOffsetX
        stimOffsetY
        numOfMonitors
        minStimTime
        meanStimTimeAdded
        minInterTrialTime
        interTrialTimeAdded
        anglePallete
        sizePallete
        gaborArray
        sigmaArray
        gaborDiameters
        pseudorandomPres
        pRandomList
        pRandomIdx
        displayInfo
        outputLine
        currentStimulusTime
        currentITI
        currentOrientation
        currentSize
        tbtSize
        tbtOrientation
        tbtITI
        tbtStimTime
    end
    
    methods
        function obj = orientationMapping(presentationData, displayInfo, outputLine)
            
            obj.spatialFreq = presentationData.spatialFreq;
            obj.temporalFreq = presentationData.temporalFreq;
            obj.circular = presentationData.circular;
            obj.solidCircle = presentationData.solidCircle; 
            obj.contrast = presentationData.contrast;
            obj.stimOffsetX = presentationData.stimOffsetX;
            obj.stimOffsetY = presentationData.stimOffsetY;
            obj.numOfMonitors = presentationData.numOfMonitors;
            obj.minStimTime = presentationData.minStimTime;
            obj.meanStimTimeAdded = presentationData.meanStimTimeAdded;
            obj.minInterTrialTime = presentationData.minInterTrialTime;
            obj.interTrialTimeAdded = presentationData.interTrialTimeAdded;
            obj.anglePallete = presentationData.anglePallete;
            obj.sizePallete = presentationData.sizePallete;
            
            obj.pseudorandomPres = presentationData.pseudorandomPresentation;
            if obj.pseudorandomPres
                obj.preparePseudorandomCombinations;
            end
            
            obj.displayInfo = displayInfo;
            obj.outputLine = outputLine;
            
            obj.gaborArray = nan(length(obj.sizePallete),2);
            obj.sigmaArray = nan(length(obj.sizePallete),1);
            
            screenWidth = displayInfo.screenSettings.disp(1);
            screenHeight = displayInfo.screenSettings.disp(2);
            for ii = 1:length(obj.sizePallete)
                if obj.sizePallete(ii) <= 1
                    obj.gaborDiameters(ii) = round(screenHeight*obj.sizePallete(ii));
                else
                    %                     gaborDiameter = obj.displayInfo.screenSettings.pixPerDeg*obj.sizePallete(ii);
                    obj.gaborDiameters(ii) = round((tand(obj.sizePallete(ii)/2)*obj.displayInfo.screenSettings.monitorDistance)*2*obj.displayInfo.screenSettings.pixPerCMwidth);
                end
                
                if obj.circular && obj.solidCircle
                    obj.sigmaArray(ii)  = 0;
                else
                    obj.sigmaArray(ii)  = obj.gaborDiameters(ii)/7;
                end
                
                if obj.circular && obj.solidCircle
                    obj.gaborArray(ii,1) = CreateProceduralSmoothedApertureSineGrating(displayInfo.winPrimary, obj.gaborDiameters(ii), obj.gaborDiameters(ii),...
                        [.5 .5 .5 0], obj.gaborDiameters(ii)/2, [], obj.sigmaArray(ii), true, 1);
                    if isfield(displayInfo, 'winSecondary')
                        obj.gaborArray(ii,2) = CreateProceduralSmoothedApertureSineGrating(displayInfo.winSecondary, obj.gaborDiameters(ii), obj.gaborDiameters(ii),...
                            [.5 .5 .5 0], obj.gaborDiameters(ii)/2, [], obj.sigmaArray(ii), true, 1);
                    end
                else
                    obj.gaborArray(ii,1) = CreateProceduralGabor(displayInfo.winPrimary, obj.gaborDiameters(ii), obj.gaborDiameters(ii), [], [0.5 0.5 0.5 0.0],1,.5);
                    if isfield(displayInfo, 'winSecondary')
                        obj.gaborArray(ii,2) = CreateProceduralGabor(displayInfo.winSecondary, obj.gaborDiameters(ii), obj.gaborDiameters(ii), [], [0.5 0.5 0.5 0.0],1,.5);
                    end
                end
            end
            
            
        end
        
        function setUpNextTrial(obj)
            if obj.pseudorandomPres && obj.pRandomIdx == size(obj.pRandomList,1)
                 obj.preparePseudorandomCombinations;
            end
            
            
            if obj.pseudorandomPres
                obj.pRandomIdx = obj.pRandomIdx + 1;
                obj.currentOrientation = obj.pRandomList(obj.pRandomIdx,1);
                obj.currentSize = obj.pRandomList(obj.pRandomIdx,2);
            else
                obj.currentOrientation = obj.anglePallete(randperm(length(obj.anglePallete),1));
                obj.currentSize = obj.sizePallete(randperm(length(obj.sizePallete),1));
            end
            
            obj.currentITI = obj.minInterTrialTime + exprnd(obj.interTrialTimeAdded);
            obj.currentStimulusTime = obj.minStimTime + exprnd(obj.meanStimTimeAdded);
            
            obj.tbtSize(end+1,1) = obj.currentSize;
            obj.tbtOrientation(end+1,1) = obj.currentOrientation;
            obj.tbtITI(end+1,1) = obj.currentITI;
            obj.tbtStimTime(end+1,1) = obj.currentStimulusTime;
        end
        
        function preparePseudorandomCombinations(obj)
            [angles, sizes] = meshgrid(obj.anglePallete, obj.sizePallete);
            allCombinations = [reshape(angles, [size(angles,1)*size(angles,2), 1]), reshape(sizes, [size(sizes,1)*size(sizes,2), 1])];
            obj.pRandomList = allCombinations(randperm(size(allCombinations,1)), :);
            obj.pRandomIdx = 0;
        end
        
        function presentStimuli(obj)
            
            aspectratio = 1;
            
            SF = obj.spatialFreq;
            period = round(obj.displayInfo.screenSettings.pixPerDeg./SF); % displayInfo.screenSettings
            cyclespersecond = obj.temporalFreq;
            
            if obj.circular
                spatialConstant = obj.sigmaArray(obj.sizePallete == obj.currentSize);
            else
                spatialConstant = inf;
            end
            
            freq = 1/period;
            trialContrast = obj.contrast;
            orientation = obj.currentOrientation;
            
            % Present a blank screen if the orientation is NaN.
            if isnan(orientation)
                trialContrast = 0;
                orientation = 180;
            end
            
            phase = rand*360; % randomise starting phase of grating
            
            screenHeight = obj.displayInfo.screenSettings.disp(2);
            screenWidth = obj.displayInfo.screenSettings.disp(1);
           
            
            % If size is between 0 and 1 use as a proportion of screen height.
            if obj.currentSize <= 1
                diameter = screenHeight*obj.currentSize;
            % Otherwise treat as number of degrees
            elseif obj.currentSize > 1
                diameter = obj.displayInfo.screenSettings.pixPerDeg*obj.currentSize;
            end
            
            botPos = (screenHeight-diameter)/2;
            topPos = diameter+botPos;
            leftPos = (screenWidth-diameter)/2;
            rightPos = diameter+leftPos;
            
            if abs(obj.stimOffsetX) <=1
                xShift = obj.stimOffsetX*obj.displayInfo.screenSettings.centre(1);
            else
                xShift = obj.stimOffsetX;
            end
            if abs(obj.stimOffsetY) <=1
                yShift = obj.stimOffsetY*obj.displayInfo.screenSettings.centre(2);
            else
                yShift = obj.stimOffsetY;
            end
            leftScreenGaborPos = [leftPos+xShift, botPos+yShift, rightPos+xShift, topPos+yShift];
            rightScreenGaborPos = [leftPos-xShift, botPos+yShift, rightPos-xShift, topPos+yShift];
            
            gabortexP = obj.gaborArray(obj.sizePallete == obj.currentSize, 1);
            if obj.circular && obj.solidCircle
                Screen('DrawTexture', obj.displayInfo.winPrimary, gabortexP, [], leftScreenGaborPos, orientation, [], [], [], [], [], [180-phase, freq, trialContrast, 0]);
                if isfield(obj.displayInfo, 'winSecondary')
                    gabortexS = obj.gaborArray(obj.sizePallete == obj.currentSize, 2);
                    Screen('DrawTexture', obj.displayInfo.winSecondary, gabortexS, [], rightScreenGaborPos, 180-orientation, [], [], [], [], [], [180-phase, freq, trialContrast, 0]);
                end
            else
                Screen('DrawTexture', obj.displayInfo.winPrimary, gabortexP, [], leftScreenGaborPos, orientation, [], [], [], [], kPsychDontDoRotation, [180-phase, freq, spatialConstant, trialContrast, aspectratio, 0, 0, 0]);
                if isfield(obj.displayInfo, 'winSecondary')
                    gabortexS = obj.gaborArray(obj.sizePallete == obj.currentSize, 2);
                    Screen('DrawTexture', obj.displayInfo.winSecondary, gabortexS, [], rightScreenGaborPos, 180-orientation, [], [], [], [], kPsychDontDoRotation, [180-phase, freq, spatialConstant, trialContrast, aspectratio, 0, 0, 0]);
                end
            end
            % photodiode patch
            Screen('FillRect', obj.displayInfo.winPrimary, obj.displayInfo.white, [0 0 120 120]);% white
            
            
            vbl = Screen('Flip', obj.displayInfo.winPrimary, [],[],[],1);
            vblStart = vbl;
            
            % Inform DAQ stimulus is being switched on
            obj.outputLine.outputSingleScan(1);
            while vbl < vblStart + obj.currentStimulusTime
                phaseincrement = (cyclespersecond * 360) * obj.displayInfo.ifi;
                phase = phase + phaseincrement;
                if obj.circular && obj.solidCircle
                    Screen('DrawTexture', obj.displayInfo.winPrimary, gabortexP, [], leftScreenGaborPos, orientation, [], [], [], [], [], [180-phase, freq, trialContrast, 0]);
                    if isfield(obj.displayInfo, 'winSecondary')
                        Screen('DrawTexture', obj.displayInfo.winSecondary, gabortexS, [], rightScreenGaborPos, 180-orientation, [], [], [], [], [], [180-phase, freq, trialContrast, 0]);
                    end
                else
                    Screen('DrawTexture', obj.displayInfo.winPrimary, gabortexP, [], leftScreenGaborPos, orientation, [], [], [], [], kPsychDontDoRotation, [180-phase, freq, spatialConstant, trialContrast, aspectratio, 0, 0, 0]);
                    if isfield(obj.displayInfo, 'winSecondary')
                        Screen('DrawTexture', obj.displayInfo.winSecondary, gabortexS, [], rightScreenGaborPos, 180-orientation, [], [], [], [], kPsychDontDoRotation, [180-phase, freq, spatialConstant, trialContrast, aspectratio, 0, 0, 0]);
                    end
                end
                % photodiode patch
                Screen('FillRect', obj.displayInfo.winPrimary, obj.displayInfo.white, [0 0 120 120]);% white
                vbl = Screen('Flip', obj.displayInfo.winPrimary, vbl + 0.5 * obj.displayInfo.ifi,[],[],1);
            end
            
            Screen('FillRect', obj.displayInfo.winPrimary, obj.displayInfo.grey);
            if isfield(obj.displayInfo, 'winSecondary'); Screen('FillRect', obj.displayInfo.winSecondary, obj.displayInfo.grey); end
            
            % photodiode patch
            Screen('FillRect',obj.displayInfo.winPrimary, obj.displayInfo.black, [0 0 120 120]);% Black
            %Screen('Flip', fsm.winL, vbl + 0.5 * fsm.ifi,[],1,1);
            
            % Inform DAQ stimulus is being switched off
            obj.outputLine.outputSingleScan(0);
            Screen('Flip', obj.displayInfo.winPrimary, [],[],[],1);
            
%             singleTrialData.leftScreenGaborPos = leftScreenGaborPos;
%             singleTrialData.rightScreenGaborPos = rightScreenGaborPos;
        end
    end
end

