function outputRange = convertListToNum(inputList)
    if any(contains(inputList, {'%', '=', '/', '\', '(',')'}))
        error('Please provide only a list of numbers as an input for the size and orientation fields.')
    else
        outputRange = eval(sprintf('[%s]', inputList));
    end
end