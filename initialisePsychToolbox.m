  function displayInfo = initialisePsychToolbox(defaultSpatialFreq, screenIDs, twoMonitors, gammaCorrect, monitorDistance, monitorWidth, monitorHeight, photodiodeSquare)

arguments
    defaultSpatialFreq double
    screenIDs double
    twoMonitors logical = 0
    gammaCorrect logical = 0
    monitorDistance double = 20
    monitorWidth double = 47.5
    monitorHeight double = 30
    photodiodeSquare double = [0 0 120 120]
end


%when working with the PTB it is a good idea to enclose the whole body of your program
%in a try ... catch ... end construct. This will often prevent you from getting stuck
%in the PTB full screen mode
try
    % Make sure this is running on OpenGL Psychtoolbox:
    AssertOpenGL;
    
% ------ Add this in when you get a chance to test - DMJ -------------------%
%     s = daq.createSession('ni');
%     s.addDigitalChannel('Dev1', 'Port0/Line4', 'OutputOnly'); % stimulus bits to auxiliary computer
%     s.outputSingleScan(0); % first call is slowest
    
% PsychDefaultSetup(2);

    % Disable synctests for this quick demo:
    displayInfo.oldSyncLevel = Screen('Preference', 'SkipSyncTests', 2);
    
    % Choose screens:
    allscreens = Screen('Screens');
%     if length(allscreens)>3 && twoMonitors
%         screenidPrimary = allscreens(end-1);
%         screenidSecondary = allscreens(end);
%     else
%         screenidPrimary = allscreens(end);
%     end
        
    screenidPrimary = screenIDs(1);
    if length(screenIDs) == 2 && twoMonitors
        screenidSecondary = screenIDs(2);
    end
    
     rect = [];
    
     % Define black and white (white will be 1 and black 0).
     white = WhiteIndex(screenidPrimary);
     black = BlackIndex(screenidPrimary);
     grey = white/2;
     
     
    % Open a fullscreen onscreen window on that display, choose a background
    % color of 128 = gray with 50% max intensity:
    [winPrimary,screenRect] = Screen('OpenWindow', screenidPrimary, 128, rect);
    if twoMonitors
        [winSecondary,screenRect] = Screen('OpenWindow', screenidSecondary, 128, rect);
    end
    ifi = Screen('GetFlipInterval', winPrimary);
    
    
    if gammaCorrect
        % Load gamma correction table
        %load ('C:\Users\KhanLab\Documents\MATLAB\FSM_KCL\gamma correction\gamma correction mesoscope\calib20170817_DellU2715H_bright50_cont50.mat')
        load ('C:\Users\KhanLab\Documents\MATLAB\FSM_KCL\gamma correction\gamma correction KCL 2p\calib20190430_DellU2715Hc_bright50_cont50.mat');
        Screen('LoadNormalizedGammaTable', winPrimary, GammaTable'*[1 1 1]);
        if twoMonitors; Screen('LoadNormalizedGammaTable', winSecondary, GammaTable'*[1 1 1]);end
    end
    
    % Screen parameters:
    
    scrset.disp = [screenRect(3),screenRect(4)];
    scrset.centre = [scrset.disp(1)/2,scrset.disp(2)/2];
    scrset.fp = scrset.centre;
    
    
    scrset.monitorDistance = monitorDistance; % distance of monitor (cm)
    scrset.monitorWidth    = monitorWidth; % Dell P2210: monitor width (cm) 47.5 x 30 cm
    scrset.monitorHeight = monitorHeight;
    
    scrset.pixPerCMwidth = scrset.disp(1)/monitorWidth;
    scrset.pixPerCMheight = scrset.disp(2)/monitorHeight;
    
    scrset.pixPerDeg = (( 0.5*scrset.disp(1)) / (atand((0.5*scrset.monitorWidth)./scrset.monitorDistance))); % number of pixels per degree Elmain2.cpp Line 880
    scrset.pixPerDegHeight = (( 0.5*scrset.disp(2)) / (atand((0.5*scrset.monitorHeight)./scrset.monitorDistance)));
    
    SF = defaultSpatialFreq;
    period   = round(scrset.pixPerDeg./SF);
    phase = 0; %phasesteps(T); phasesteps = pi:(2.*pi.*Steps):3*pi; % use single phase (and translate later)
    
    
    % Initial stimulus params for the gabor patch:
    res = [scrset.disp, scrset.disp];% monior y resolution 4*[323 323];

    sc = inf;%250.0; % spatial const of gaussian
    freq = 1/period;
    tilt = 0;
    contrast = 0; % to start with gray
    aspectratio = 1;
    
    tw = res(1);
    th = res(2);
    
    
    % Build a procedural gabor texture for a gabor with a support of tw x th
    % pixels, and a RGB color offset of 0.5 -- a 50% gray.
    %gabortexP = CreateProceduralGabor(fsm.winL, tw, th, 0, [0.5 0.5 0.5 0.0],1,.5);
    %gabortexS = CreateProceduralGabor(fsm.winR, tw, th, 0, [0.5 0.5 0.5 0.0],1,.5);
    gabortexP = CreateProceduralGabor(winPrimary, tw, th, 0, [0.5 0.5 0.5 0.0],1,.5);
    if twoMonitors 
        gabortexS = CreateProceduralGabor(winSecondary, tw, th, 0, [0.5 0.5 0.5 0.0],1,.5);
    end
    
%     %%% for a checkerboard stim %%%
%     % Define a simple 8 by 8 checker board
%     checkerboard = 255*repmat(eye(2), 2, 2);
%     
%     % Make the checkerboard into a texure (4 x 4 pixels)
%     fsm.checkerTextureL = Screen('MakeTexture', fsm.winL, checkerboard);
%     if fsm.twomonitors;fsm.checkerTextureR = Screen('MakeTexture', fsm.winR, checkerboard);end
%     [s1, s2] = size(checkerboard);
%     baseRect = [0 0 s1 s2] .* 190;% scale
%     [xCenter, yCenter] = RectCenter(screenRect);
%     fsm.dstRect = CenterRectOnPointd(baseRect, xCenter, yCenter); % centered
%     
    
    % Draw the gabor once, just to make sure the gfx-hardware is ready for the
    % benchmark run below and doesn't do one time setup work inside the
    % benchmark loop: See below for explanation of parameters...
    Screen('DrawTexture', winPrimary, gabortexP, [], [], tilt, [], [], [], [], kPsychDontDoRotation, [phase, freq, sc, contrast, aspectratio, 0, 0, 0]);
    if twoMonitors
        Screen('DrawTexture', winSecondary, gabortexS, [], [], tilt, [], [], [], [], kPsychDontDoRotation, [phase, freq, sc, contrast, aspectratio, 0, 0, 0]);
    end
    
    % Store the variables we might need later in displayInfo
    displayInfo.winPrimary = winPrimary;
    displayInfo.gabortexP = gabortexP;

    if twoMonitors
        displayInfo.winSecondary = winSecondary;
        displayInfo.gabortexS = gabortexS;
    end
    
%     displayInfo.spatialConst = sc;
    displayInfo.phase = phase;
    displayInfo.screenSettings = scrset;
    displayInfo.ifi = ifi;
    displayInfo.white = white;
    displayInfo.grey = grey;
    displayInfo.black = black;
    displayInfo.photodiodeSquare = photodiodeSquare;
    
    
    % Perform initial flip to gray background and sync us to the retrace:
    pause(0.1)
    Screen('FillRect', winPrimary, black, photodiodeSquare)
    Screen('Flip', winPrimary,[],[],1,1); % with multiflip
    drawnow; 
    pause(0.001);
    return
    
catch
    % This section is executed only in case an error happens in the
    % experiment code implemented between try and catch...
    ShowCursor;
    Screen('CloseAll');
    %Screen('Preference', 'SkipSyncTests', fsm.oldSyncLevel);
    psychrethrow(psychlasterror); %output the error message
end


end