classdef gratings < observationCore
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = gratings(aiFileName, stimFileName)
            %UNTITLED3 Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@observationCore(aiFileName, stimFileName);
            
            obj.data.sel.orientation = obj.stimData.orientation;
            obj.data.sel.stimSize = obj.stimData.stimSize;
            obj.data.sel.laserPower = zeros(length(obj.data.sel.orientation) ,1);
            
            % Optogenetic laser is not currently implemented
            laserPowerList = 0;
            conditionLabelTemp = cell(length(obj.data.sel.orientation) ,1);
            for ii=1:length(obj.data.sel.orientation) 
               conditionLabelTemp{ii}=sprintf('o%1.2f_s%1.2f_L%1.2f', obj.sel.orientation(ii), obj.sel.stimSize(ii), laserPowerList);
            end
            
            [cB,cI,cJ] = unique(conditionLabelTemp);
            
            obj.data.sel.condAll = cJ;
            % if total n unique stims, sel.condition tells you which one (1 to n) for each trigger event, as index to sel.conditionLabel below
            obj.data.sel.condAllLabel    = conditionLabelTemp(cI);% names of each of the n unique stim conditions (including orientation and (optional) laser) eg vis1_Irrel at 170degrees with 40% laser will be vis1Irrel_170.00_40
            
            obj.data.sel.combi = [];
            obj.data.sel.combi = NaN(length(obj.stimData.anglePallete),length(obj.stimData.sizePallete));
            obj.data.sel.combi2ori = obj.data.sel.combi;
            obj.data.sel.combi2size = obj.data.sel.combi;
            for ii=1:length(obj.stimData.anglePallete)
                for jj=1:length(obj.stimData.sizePallete)
                    obj.data.sel.combi(ii,jj) = unique(obj.data.sel.condition(obj.data.sel.orientation==obj.stimData.anglePallete(ii) & obj.data.sel.stimSize==obj.stimData.sizePallete(jj)));
                    obj.data.sel.combi2ori(ii,jj) = obj.stimData.anglePallete(ii);
                    obj.data.sel.combi2size(ii,jj) = obj.stimData.sizePallete(jj);
                end
            end
            
        end
        
    end
end

