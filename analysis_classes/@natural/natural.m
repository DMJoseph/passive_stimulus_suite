classdef natural < observationCore
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = natural(aiFileName, stimFileName)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@observationCore(aiFileName, stimFileName);
            obj.data.sel.imageID = obj.stimData.imageID;
            obj.data.sel.uniqueIDs = obj.stimData.stimCore.imageIDs;
        end
        
    end
end

