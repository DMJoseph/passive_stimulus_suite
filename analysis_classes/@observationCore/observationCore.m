classdef observationCore
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        mouseID
        dataAI
        stimData
        data
        issue
    end
    
    methods
        function bhv = observationCore(aiFileName, stimFileName)
            
            recordedData = readAIrecorderBinFile(aiFileName);
            % Get the right bitnum for converting the data
            recordedData.bitNum = str2double(recordedData.dataType(end-1:end));
            recordedData.data = recordedData.data*recordedData.voltageRange/2^(recordedData.bitNum-1);
            
            % Get the session name information - helpful for saving things.
            splitFilename = regexp(recordedData.fname(1:end-4), '[\\/]','split');
            bhv.mouseID = splitFilename{end};% mouse name etc
            
            %% Find frame triggers, reward, grating, odour and lick channel on and off times
            
            % Get the index of the different channels
            frame_trigger = strmatch('frame_trigger',recordedData.chanNames);
            runningChan  =  strmatch('running',recordedData.chanNames);
            laser        =  strmatch('laser',recordedData.chanNames);
            mapping      =  strmatch('mapping_stim',recordedData.chanNames);
            
            % Get the times of each sample in the AI recorder data.
            tStep = 1/recordedData.sampleRate;
            tFinal = (size(recordedData.data,1)-1)*tStep;
            T = (0:tStep:tFinal)';
            
            % From Teensy, speed in cm/s = (outputVoltage - .4)/.016
            speed = (recordedData.data(:,runningChan)-.4)/.016;
            
            channelsList = [frame_trigger, laser, mapping];
            
            
            % Find stim times etc
            for channels = channelsList
                % Find the maximum and minimum values of the channel and automatically determine a threshold for onset pulses.
                maxVal = max(recordedData.data(:,channels));
                minVal = min(recordedData.data(:,channels));
                thresh = (maxVal-minVal)*0.5+minVal;
                % If the differences are too small, the channel was likely silent. Automatically set a threshold.
                if (maxVal-minVal)<0.3; thresh = 1; end
                
                % Find the onsets and offsets of the channel.
                onIndex = find(diff(recordedData.data(:,channels)>thresh)==1);
                offIndex = find(diff(recordedData.data(:,channels)>thresh) == -1);
                
                % Dealing with some cases like the frame trigger where there may be a final on transition without a corresponding off.
                if length(onIndex)>length(offIndex)
                    if all(recordedData.data(onIndex(end)+1:end,channels)>thresh)
                        warning('off','backtrace');
                        warning('%s channel has an on signal without a corresponding off. Deleting the last on transition.',recordedData.chanNames{channels})
                        onIndex(end) = [];
                    end
                end
                
                % Add the detected on and off transitions to the workspace.
                % - creates variables called rewdOnT etc
                eval(sprintf('%sOnT=T(onIndex);',recordedData.chanNames{channels}));
                eval(sprintf('%sOffT=T(offIndex);',recordedData.chanNames{channels}));
            end
            
            bhv.dataAI.time = T;
            bhv.dataAI.speed = speed;
            bhv.dataAI.frame_triggerOnT = frame_triggerOnT;
            bhv.dataAI.frame_triggerOffT = frame_triggerOffT;
            bhv.dataAI.laserOnT = laserOnT;
            bhv.dataAI.laserOffT = laserOffT;
            bhv.dataAI.mapping_stimOnT = mapping_stimOnT;
            bhv.dataAI.mapping_stimOffT = mapping_stimOffT;
            
            bhv.data.sel.trltim = mapping_stimOnT;
            
            load(stimFileName, 'saveData')
            bhv.stimData = saveData;
            
            %% Running alignment
            % Picks out times when the mouse started running and maintained it for a minimum time.
            % Picks out what was happening in the minimum time after the start of each running bout.
            
            potentialOnsets = find(diff(speed>5)==1);
            stillT = 0.2/tStep; % The time the mouse had to be still for before running - 200ms.
            minRunT = 1/tStep; % The minumum time the mouse had to be running for - 1s.
            endOffset = 10/tStep; % The amount of time that has to be remaining before the end of the session - 10s.
            
            counter = 1;
            
            runningOnsets = nan;
            
            for ii = 1:length(potentialOnsets)
                %Is the mouse moving at less than 5cm/s for the previous 200ms? If not
                %reject the potential onset of running.
                if potentialOnsets(ii)-stillT < 0
                    % Check we aren't looking for a time before the start of the recording.
                    continue
                end
                % Check the mouse wasn't running beforehand and that there is enough time left in the recording.
                if sum(speed(potentialOnsets(ii)-stillT : potentialOnsets(ii))<5) > (stillT*0.9) && potentialOnsets(ii)+endOffset < length(T)
                    % Check that the mouse maintained running and that we aren't within the minimum running time of a previous running bout.
                    if sum(speed(potentialOnsets(ii) : potentialOnsets(ii)+minRunT)>5) > (minRunT*0.9) && (potentialOnsets(ii)-runningOnsets(end) > minRunT || isnan(runningOnsets(end)))
                        runningOnsets(counter, 1) = potentialOnsets(ii);
                        counter = counter+1;
                    end
                end
            end
            
            
            if ~isnan(runningOnsets)
                minFramesBeforeRunning = 10;
                % If it was an imaging session discard any running onsets that happen within the first few frames. It stops errors later.
                if ~isempty(frame_triggerOnT) && any(T(runningOnsets)<frame_triggerOnT(minFramesBeforeRunning))
                    firstAcceptableRun = runningOnsets(find(T(runningOnsets)>frame_triggerOnT(minFramesBeforeRunning),1));
                    runningOnsets = runningOnsets(runningOnsets>=firstAcceptableRun);
                end
                
                run.runningOnsets = T(runningOnsets);
                run.runningIndex = observationCore.calculateRunningIndex(T, run.runningOnsets, minRunT*tStep, mapping_stimOnT, mapping_stimOffT);
                
            else
                run.runningOnsets = runningOnsets;
                run.runningIndex = nan;
            end
            
            run.stillT = stillT*tStep;
            run.minRunT = minRunT*tStep;
            
            %% Extract running traces around each of the prospective running bouts
            [runOnsetTraces, runT] = observationCore.extractRunningTraces(T, speed, run.runningOnsets, recordedData.sampleRate);
            run.runOnsetTraces = runOnsetTraces;
            run.relativeRunTime = runT;
            
            %% Extract running traces around each stimulus presentation
            
            run.allTrial = behaviourCore.extractRunningTraces(T, speed, sel.trltim, recordedData.sampleRate);
            
            bhv.data.run = run;
        end
    end
    
    % Define methods used by the constructor function.
    methods(Static, Hidden, Access = protected)
        %% Function to find out what was happening in each of the running bouts.
        function runningIndex = calculateRunningIndex(time, runningOnsets, minRunT, onTimes, offTimes)
            
            runningIndex = cell(length(runningOnsets),1);
            conditionLabelledTime = zeros(length(time),1);
            
            for jj = 1:length(onTimes)
                conditionLabelledTime(time>onTimes(jj) & time<offTimes(jj)) = 1;
            end
            
            for kk = 1:length(runningOnsets)
                runCondition = unique(conditionLabelledTime(time>runningOnsets(kk) & time<(runningOnsets(kk)+minRunT)));
                runningIndex{kk} = runCondition;
            end
            
        end
        
        %%
        function [tracesAllTrials, relativeRunT] = extractRunningTraces(time, speed, stimTimes, sampleRate)
            
            extractT = 5;
            sampleRange = (extractT*-1*sampleRate):(extractT*sampleRate);
            numOfSamples = length(sampleRange);
            relativeRunT = sampleRange./sampleRate;
            tracesAllTrials = nan(length(stimTimes),numOfSamples);
            
            
            for ii = 1:length(stimTimes)
                trialRunning = speed(time<(stimTimes(ii)+extractT) & time>(stimTimes(ii)-extractT));
                endIndex = min(length(trialRunning), numOfSamples);
                tracesAllTrials(ii,1:endIndex) = trialRunning(1:endIndex);
            end
        end
    end
end

